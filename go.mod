module gitee.com/Cookie_XiaoD/dna

go 1.16

require (
	github.com/samuel/go-zookeeper v0.0.0-20201211165307-7117e9ea2414
	github.com/sony/sonyflake v1.0.0
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
)

# go-dna

#### 介绍

分布式唯一ID生成服务的golang实现。   
世上不存在完全相同的DNA，如同该服务提供的唯一ID一样

#### 软件架构

参考美团Leaf方案实现  
https://tech.meituan.com/2017/04/21/mt-leaf.html

snowflake：
![img.png](snowflake.png)
时间戳在前，整体趋势递增

#### 集成

直接集成核心实现分布式ID服务而非使用现有dna服务端非常容易   
首先进行安装  
go get -u gitee.com/Cookie_XiaoD/dna  
随后参考示例

```golang
package main

import (
	"gitee.com/Cookie_XiaoD/dna/core/snowflake"
	"log"
)

func main() {
	//初始化
	if err := snowflake.Init([]string{"127.0.0.1:2181"}, snowflake.WithNodeEndpoint("127.0.0.1:10001")); err != nil {
		log.Fatal(err)
	}
	//在需要的地方获取ID，将其包装为web api或rpc服务等即可
	id, err := snowflake.GetID()
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(id)
}

```

#### 事项

- [x] 基于zk实现类似leaf-snowflake的方案
- [x] 提供基础的GRPC服务
- [ ] 增加自定义日志，替换log
- [ ] dna服务增加配置文件替代flag
- [ ] 集群时间校准改为rpc通信
- [ ] 替换sonyflake包(每毫秒可生成25~30个ID，性能一般)
- [ ] snowflake方案压测
- [ ] 基于mysql实现类似leaf-segment的方案
- [ ] 增加单调递增方案




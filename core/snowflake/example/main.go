package main

import (
	"gitee.com/Cookie_XiaoD/dna/core/snowflake"
	"log"
)

func main() {
	//初始化
	if err := snowflake.Init([]string{"127.0.0.1:2181"}, snowflake.WithNodeEndpoint("127.0.0.1:10001")); err != nil {
		log.Fatal(err)
	}
	//在需要的地方获取ID，将其包装为web api或rpc服务等即可
	id, err := snowflake.GetID()
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(id)
}
